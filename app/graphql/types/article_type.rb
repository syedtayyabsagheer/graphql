module Types
  class ArticleType < Types::BaseObject
    field :id, ID, null: false
    field :url, String, null: false
    field :description, String, null: false
    field :title, String, null: false
  end
end
